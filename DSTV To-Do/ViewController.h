//
//  ViewController.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/20.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentController.h"
#import "EmailHelper.h"
#import "AlertControllers/GeneralErrorAlert/GeneralErrorAlert.h"
#import "WebConnections/ConnectionHelper/ConnectionHelper.h"
#import "User.h"
#import "ApiResponse.h"
#import "HomeController.h"

@interface ViewController : ParentController

@property (weak, nonatomic) IBOutlet UITextField *emailAddress;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;


@end

