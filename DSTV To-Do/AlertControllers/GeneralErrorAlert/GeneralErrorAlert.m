//
//  GeneralErrorAlert.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/20.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "GeneralErrorAlert.h"

@implementation GeneralErrorAlert


+(void)showErrorAlert:(NSString*)title message:(NSString*)message viewController:(UIViewController*)viewController {
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* alertAction = [UIAlertAction actionWithTitle:@"Ok"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:alertAction];
    [viewController presentViewController:alertController animated:true completion:nil];
    
}

@end
