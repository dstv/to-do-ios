//
//  GeneralErrorAlert.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/20.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GeneralErrorAlert : NSObject

+(void)showErrorAlert:(NSString*)title message:(NSString*)message viewController:(UIViewController*)viewController;

@end

NS_ASSUME_NONNULL_END
