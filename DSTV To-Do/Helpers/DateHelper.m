//
//  DateHelper.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "DateHelper.h"

@implementation DateHelper

+ (NSString*)formatDate:(NSString*)dateFromServer {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateFromServer];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    return [dateFormat stringFromDate:date];
}

+(NSDate*)getDateFromString:(NSString*)dateFromServer {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    return [dateFormat dateFromString:dateFromServer];
}

+ (NSString*)formatDateForPOST:(NSDate*)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    return [dateFormat stringFromDate:date];
}

+ (NSString*)getCurrentDateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    return [formatter stringFromDate:[NSDate date]];
}

@end
