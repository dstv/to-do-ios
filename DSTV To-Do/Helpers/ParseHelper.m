//
//  ParseHelper.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "ParseHelper.h"

@implementation ParseHelper


+ (NSDictionary*)dictionaryWithPropertiesOfObject:(id)obj {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];

    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);

    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        [dict setObject:[obj valueForKey:key] ? [obj valueForKey:key] : @"" forKey:key];
    }

    free(properties);

    return [NSDictionary dictionaryWithDictionary:dict];
}

@end
