//
//  DateHelper.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DateHelper : NSObject

+ (NSString*)formatDate:(NSString*)dateFromServer;
+ (NSDate*)getDateFromString:(NSString*)dateFromServer;
+ (NSString*)formatDateForPOST:(NSDate*)date;
+ (NSString*)getCurrentDateString;

@end

NS_ASSUME_NONNULL_END
