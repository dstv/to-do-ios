//
//  ParseHelper.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

NS_ASSUME_NONNULL_BEGIN

@interface ParseHelper : NSObject

+ (NSDictionary*)dictionaryWithPropertiesOfObject:(id)obj;

@end

NS_ASSUME_NONNULL_END
