//
//  EmailHelper.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/20.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmailHelper : NSObject

+(BOOL)isEmailValid:(NSString*)emailAddress;

@end



NS_ASSUME_NONNULL_END
