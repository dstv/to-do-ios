//
//  ParentController.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/20.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "ParentController.h"

@interface ParentController ()

@end

@implementation ParentController {
    UITextField* activeTextField;
    CGPoint scrollPoint;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification {
    
    // Step 1: Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // Step 3: Scroll the target text field into view. But first check it is behind the keyboard, else do nothing
    if ((activeTextField.frame.origin.y + activeTextField.frame.size.height) <= keyboardSize.height ) {
        
        CGRect aRect = self.view.frame;
        aRect.size.height -= keyboardSize.height;
        scrollPoint = CGPointMake(0.0, activeTextField.superview.frame.origin.y + activeTextField.frame.origin.y
                                  - (keyboardSize.height - activeTextField.frame.size.height));
        [UIView animateWithDuration:0.25 animations:^ {
            CGRect newFrame = [self.view frame];
            newFrame.origin.y -= self->scrollPoint.y; // tweak here to adjust the moving position
            [self.view setFrame:newFrame];
        } completion:^(BOOL finished) {
            
        }];
    }
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    [UIView animateWithDuration:0.25 animations:^ {
        CGRect newFrame = [self.view frame];
        newFrame.origin.y += self->scrollPoint.y; // tweak here to adjust the moving position
        [self.view setFrame:newFrame];
    } completion:^(BOOL finished) {
        
    }];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    activeTextField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
