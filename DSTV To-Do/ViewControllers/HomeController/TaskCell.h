//
//  TaskCell.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
#import "TaskTableHelper.h"
#import "TasksApiHelper.h"

NS_ASSUME_NONNULL_BEGIN

@interface TaskCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *isCompleteSwitch;
@property (weak, nonatomic) IBOutlet UILabel *descriptionText;
@property (weak, nonatomic) IBOutlet UILabel *dateText;
@property (weak, nonatomic) IBOutlet UIButton *isImportantButton;

@end

NS_ASSUME_NONNULL_END
