//
//  TaskCell.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "TaskCell.h"

@implementation TaskCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)setAsImportant:(id)sender {
    UIButton* tappedButton = (UIButton*)sender;
    Task* task = [[Task alloc] init];
    [task setValuesForKeysWithDictionary:[[[TaskTableHelper getInstance] getItems] objectAtIndex:tappedButton.tag]];
    task.isImportant = task.isImportant ? false : true;
    [[TasksApiHelper getInstance] updateTaskItem:task];
}

- (IBAction)isCompleteChanged:(id)sender {
    UISwitch* tappedButton = (UISwitch*)sender;
    Task* task = [[Task alloc] init];
    [task setValuesForKeysWithDictionary:[[[TaskTableHelper getInstance] getItems] objectAtIndex:tappedButton.tag]];
    task.isComplete = task.isComplete ? false : true;
    [[TasksApiHelper getInstance] updateTaskItem:task];
}

@end
