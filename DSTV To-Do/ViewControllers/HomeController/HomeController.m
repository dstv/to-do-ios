//
//  HomeController.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "HomeController.h"

@interface HomeController ()

@end



@implementation HomeController {
    NSMutableArray* allTasks;
    TaskTableHelper* taskTableHelper;
    TasksApiHelper* tasksApiHelper;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.loadingView startAnimating];
    [self.loadingView setHidesWhenStopped:true];
    
    taskTableHelper = [[TaskTableHelper alloc] initWithTableViewAndController:self.tableView
                                                               viewController:self
                                                                navController:self.navigationController];
    
    tasksApiHelper = [[TasksApiHelper alloc] initWithControllerAndTableHelper:self
                                                                  tableHelper:taskTableHelper];
    [tasksApiHelper getTasksList];
}

- (void)getTasks {
    [ConnectionHelper GET:[NSString stringWithFormat:@"%@%@%@",
                           NSLocalizedString(@"server", @"Server address"),
                           NSLocalizedString(@"get_tasks_by_email", @"get user tasks"),
                           [[NSUserDefaults standardUserDefaults] objectForKey:@"userEmail"]]
          completionBlock:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
        [self.loadingView stopAnimating];
        if ([responseObject objectForKey:@"result"]) {
            
        } else {
            [GeneralErrorAlert showErrorAlert:@"Oops!"
                                      message:[error localizedDescription]
                               viewController:self];
        }
    }];
}


- (void)showLoading {
    [self.loadingView setHidden:false];
    [self.loadingView startAnimating];
}

- (void)hideLoading {
    [self.loadingView stopAnimating];
}

- (IBAction)openAddNewTask:(id)sender {
    [taskTableHelper openTaskDetailController:nil];
}

- (IBAction)signOutAction:(id)sender {
    [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
    UIViewController *rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"login_vc"];
    [[[UIApplication sharedApplication] windows] firstObject].rootViewController = rootViewController;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
