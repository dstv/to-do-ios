//
//  HomeController.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParentController.h"
#import "TaskCell.h"
#import "GeneralErrorAlert.h"
#import "ConnectionHelper.h"
#import "Task.h"
#import "ApiResponse.h"
#import "TaskTableHelper.h"
#import "TasksApiHelper.h"
#import "TaskDetailController.h"


NS_ASSUME_NONNULL_BEGIN

@interface HomeController : ParentController 
@property (weak, nonatomic) IBOutlet UITextField *searchText;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *progressText;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;

- (void)showLoading;
- (void)hideLoading;

@end

NS_ASSUME_NONNULL_END
