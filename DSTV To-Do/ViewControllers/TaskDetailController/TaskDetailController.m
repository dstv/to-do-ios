//
//  TaskDetailController.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "TaskDetailController.h"

@interface TaskDetailController ()

@end

@implementation TaskDetailController {
    TasksApiHelper* tasksApiHelper;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.loadingView setHidesWhenStopped:true];
    tasksApiHelper = [TasksApiHelper getInstance];
    [tasksApiHelper setDetailViewController:self];
    if (self.task != nil) {
        [self updateUI];
    }
}

- (void)updateUI {
    [self.updateSaveButton setTitle:@"Update" forState:UIControlStateNormal];
    [self.removeTaskButton setHidden:false];
    self.isImportantSwitch.on = self.task.isImportant;
    self.taskDescription.text = self.task.description;
    [self.taskDueDate setDate:[DateHelper getDateFromString:self.task.reminderDate]];
}

- (IBAction)updateOrSaveAction:(id)sender {
    if (self.task != nil) {
        self.task.description = self.taskDescription.text;
        self.task.reminderDate = [DateHelper formatDateForPOST:self.taskDueDate.date];
        self.task.isImportant = self.isImportantSwitch.on;
    } else {
        self.task = [[Task alloc] init];
        self.task.description = self.taskDescription.text;
        self.task.createdDate = [DateHelper getCurrentDateString];
        self.task.reminderDate = [DateHelper formatDateForPOST:self.taskDueDate.date];
        self.task.isImportant = self.isImportantSwitch.on;
        self.task.isComplete = false;\
        self.task.id = [NSNumber numberWithInt:0];
        NSNumber *userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
        self.task.userID = [userID intValue];
    }
    
    [tasksApiHelper updateTaskItemFromDetail:self.task];
    
}

- (void)showLoading {
    [self.loadingView setHidden:false];
    [self.loadingView startAnimating];
}

- (void)hideLoading {
    [self.loadingView stopAnimating];
}

- (IBAction)removeTaskAction:(id)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Remove task"
                                                                   message:@"Are you sure you want to remove this task?"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction* deleteAction = [UIAlertAction actionWithTitle:@"Remove"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
        [self->tasksApiHelper deleteTask:[self.task.id stringValue]];
    }];
    
    [alert addAction:cancelAction];
    [alert addAction:deleteAction];
    [self presentViewController:alert animated:true completion:nil];
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
