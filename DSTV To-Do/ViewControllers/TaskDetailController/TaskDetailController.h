//
//  TaskDetailController.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
#import "DateHelper.h"
#import "TasksApiHelper.h"

NS_ASSUME_NONNULL_BEGIN

@interface TaskDetailController : UIViewController

@property (nonatomic, strong) Task* task;

@property (weak, nonatomic) IBOutlet UITextField *taskDescription;
@property (weak, nonatomic) IBOutlet UIDatePicker *taskDueDate;
@property (weak, nonatomic) IBOutlet UISwitch *isImportantSwitch;
@property (weak, nonatomic) IBOutlet UIButton *updateSaveButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingView;
@property (weak, nonatomic) IBOutlet UIButton *removeTaskButton;

- (void)showLoading;
- (void)hideLoading;

@end

NS_ASSUME_NONNULL_END
