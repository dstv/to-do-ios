//
//  ViewController.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/20.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.emailAddress.delegate = self;
    [self.loadingView setHidesWhenStopped:true];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userEmail"] != nil) {
        [self openHomeController];
    }
}

- (IBAction)continueToApp:(id)sender {
    if (![EmailHelper isEmailValid:self.emailAddress.text]) {
        [GeneralErrorAlert showErrorAlert:@"Oops!"
                                  message:@"Invalid email entered, please enter a valid email address."
                           viewController:self];
    } else {
        // Make the call to the server
        [self.loadingView startAnimating];
        [ConnectionHelper GET:[NSString stringWithFormat:@"%@%@%@", NSLocalizedString(@"server", @"Server address"),
                               NSLocalizedString(@"add_user_by_email", @"Add user EP"), self.emailAddress.text]
              completionBlock:^(id responseObject, NSError* error) {
            [self.loadingView stopAnimating];
            if ([responseObject objectForKey:@"result"]) {
                // Open main view
                User* user = [[User alloc] init];
                [user setValuesForKeysWithDictionary:[[responseObject objectForKey:@"result"] objectForKey:@"data"]];
                [[NSUserDefaults standardUserDefaults] setObject:user.id forKey:@"userID"];
                [[NSUserDefaults standardUserDefaults] setObject:user.userEmailOrCell forKey:@"userEmail"];
                [self openHomeController];
            } else {
                [GeneralErrorAlert showErrorAlert:NSLocalizedString(@"oops", @"Oops title")
                                          message:[error localizedDescription]
                                   viewController:sender];
            }
        }];
    }
}

- (void) openHomeController {
    UINavigationController *rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"nav_vc"];
    [[[UIApplication sharedApplication] windows] firstObject].rootViewController = rootViewController;
}

@end
