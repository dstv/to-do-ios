//
//  TasksApiHelper.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "TasksApiHelper.h"

@implementation TasksApiHelper {
    HomeController* _viewController;
    TaskDetailController* _viewDetailController;
    TaskTableHelper* _tableHelper;
    NSArray* tasks;
}

static TasksApiHelper *instance = nil;
+(TasksApiHelper *)getInstance {
    @synchronized(self) {
        if (instance == nil) {
            instance= [TasksApiHelper new];
        }
    }
    return instance;
}


- (TasksApiHelper*)initWithControllerAndTableHelper:(UIViewController*)controller tableHelper:(id)tableHelper {
    _viewController = (HomeController*)controller;
    _tableHelper = tableHelper;
    instance = self;
    return self;
}

- (void)setDetailViewController:(UIViewController*)viewController {
    _viewDetailController = (TaskDetailController*) viewController;
}

- (void)getTasksList {
    [_viewController showLoading];
    [ConnectionHelper GET:[NSString stringWithFormat:@"%@%@%@",
                           NSLocalizedString(@"server", @"Server address"),
                           NSLocalizedString(@"get_tasks_by_email", @"get user tasks"),
                           [[NSUserDefaults standardUserDefaults] objectForKey:@"userEmail"]]
          completionBlock:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
        [self->_viewController hideLoading];
        if ([responseObject objectForKey:@"result"]) {
            self->tasks = [[responseObject objectForKey:@"result"] objectForKey:@"data"];
            [self->_tableHelper setNewTaskItems:self->tasks];
            [self updateTasksProgress];
            [self->_tableHelper reloadTableView];
        } else {		
            [GeneralErrorAlert showErrorAlert:@"Oops!"
                                      message:[error localizedDescription]
                               viewController:self->_viewController];
        }
    }];
}

- (void)updateTasksProgress {
    float complete = 0;
    for (int i = 0; i < tasks.count; i++) {
        Task* task = [[Task alloc] init];
        [task setValuesForKeysWithDictionary:[tasks objectAtIndex:i]];
        if (task.isComplete) {
            complete++;
        }
    }

    float progressPerc = complete / tasks.count;
    [_viewController.progressView setProgress:progressPerc];
    [_viewController.progressText setText:[NSString stringWithFormat:@"%i%%", (int)(progressPerc * 100)]];
}


- (void)updateTaskItem:(Task*)task {
    [_viewController showLoading];
    [ConnectionHelper POST:[NSString stringWithFormat:@"%@%@",
    NSLocalizedString(@"server", @"Server address"),
    NSLocalizedString(@"add_task_for_user", @"Add/Update task")]
                postObject:[ParseHelper dictionaryWithPropertiesOfObject:task]
           completionBlock:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
        [self->_viewController hideLoading];
        if ([responseObject objectForKey:@"result"]) {
            [self getTasksList];
        } else {
            [GeneralErrorAlert showErrorAlert:@"Oops!"
                                                 message:[error localizedDescription]
                                          viewController:self->_viewController];
        }
    }];
}

- (void)updateTaskItemFromDetail:(Task*)task {
    [_viewDetailController showLoading];
    [ConnectionHelper POST:[NSString stringWithFormat:@"%@%@",
    NSLocalizedString(@"server", @"Server address"),
    NSLocalizedString(@"add_task_for_user", @"Add/Update task")]
                postObject:[ParseHelper dictionaryWithPropertiesOfObject:task]
           completionBlock:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
        [self->_viewDetailController hideLoading];
        if ([responseObject objectForKey:@"result"]) {
            [self->_viewDetailController.navigationController popViewControllerAnimated:true];
            [instance getTasksList];
        } else {
            [GeneralErrorAlert showErrorAlert:@"Oops!"
                                                 message:[error localizedDescription]
                                          viewController:self->_viewController];
        }
    }];
}

- (void)deleteTask:(NSString*)taskId {
    [_viewDetailController showLoading];
    [ConnectionHelper GET:[NSString stringWithFormat:@"%@%@%@",
    NSLocalizedString(@"server", @"Server address"),
    NSLocalizedString(@"delete_task_by_id", @"Delete task"),
                           taskId]
          completionBlock:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
        [self->_viewDetailController hideLoading];
        if ([responseObject objectForKey:@"result"]) {
            [self->_viewDetailController.navigationController popViewControllerAnimated:true];
            [instance getTasksList];
        } else {
            [GeneralErrorAlert showErrorAlert:@"Oops!"
                                                 message:[error localizedDescription]
                                          viewController:self->_viewController];
        }
    }];
}

@end
