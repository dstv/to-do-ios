//
//  TasksApiHelper.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeController.h"
#import "GeneralErrorAlert.h"
#import "Task.h"
#import "ApiResponse.h"
#import "TaskTableHelper.h"
#import "ParseHelper.h"
#import "TaskDetailController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TasksApiHelper : NSObject


- (TasksApiHelper*)initWithControllerAndTableHelper:(UIViewController*)controller
                                        tableHelper:(id)tableHelper;
- (void)setDetailViewController:(UIViewController*)viewController;

- (void)getTasksList;

- (void)updateTaskItem:(Task*)task;
- (void)updateTaskItemFromDetail:(Task*)task;
- (void)deleteTask:(NSString*)taskId;

+(TasksApiHelper *)getInstance;


@end

NS_ASSUME_NONNULL_END
