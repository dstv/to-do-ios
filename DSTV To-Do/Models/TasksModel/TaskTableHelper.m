//
//  TaskTableHelper.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "TaskTableHelper.h"

@implementation TaskTableHelper {
    NSArray* allTasks;
    UIStoryboard *storyBoard;
    
}

static NSString *CellIdentifier = @"task_cell";

static TaskTableHelper *instance = nil;
+(TaskTableHelper *)getInstance {
    @synchronized(self) {
        if (instance == nil) {
            instance= [TaskTableHelper new];
        }
    }
    return instance;
}

- (TaskTableHelper*)initWithTableViewAndController:(UITableView*)tableView viewController:(UIViewController*)viewController navController:(UINavigationController*)navController {
    instance = self;
    _navController = navController;
    _viewController = viewController;
    _tableView = tableView;
    [_tableView registerClass:[TaskCell class] forCellReuseIdentifier:CellIdentifier];
    [_tableView registerNib:[UINib nibWithNibName:@"TaskCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return self;
}


- (void)reloadTableView {
    [_tableView reloadData];
}

- (void)setNewTaskItems:(NSArray*)items {
    allTasks = items;
}

- (NSArray*)getItems {
    return allTasks;;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (allTasks == nil) {
        return 0;
    }
    return allTasks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TaskCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    
    Task* task = [[Task alloc] init];
    [task setValuesForKeysWithDictionary:[allTasks objectAtIndex:indexPath.row]];
    
    cell.descriptionText.text = task.description;
    cell.dateText.text = [DateHelper formatDate:task.reminderDate];
    
    // Set tags for use in helpers
    [cell.isImportantButton setTag:indexPath.row];
    [cell.isCompleteSwitch setTag:indexPath.row];
    
    if (task.isImportant) {
        [cell.isImportantButton setImage:[UIImage imageNamed:@"star_blue.png"] forState:UIControlStateNormal];
    } else {
        [cell.isImportantButton setImage:[UIImage imageNamed:@"star_grey.png"] forState:UIControlStateNormal];
    }
    
    if (task.isComplete) {
        [cell.isCompleteSwitch setOn:true];
    } else {
        [cell.isCompleteSwitch setOn:false];
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Task* task = [[Task alloc] init];
    [task setValuesForKeysWithDictionary:[allTasks objectAtIndex:indexPath.row]];
    [self openTaskDetailController:task];
}

-(void)openTaskDetailController:(Task*)task {
    TaskDetailController* taskController = (TaskDetailController *)[storyBoard instantiateViewControllerWithIdentifier:@"task_detail_vc"];
    taskController.task = task;
    [_navController pushViewController:taskController animated:true];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 73;
}


@end
