//
//  TaskTableHelper.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/24.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TaskCell.h"
#import "Task.h"
#import "DateHelper.h"
#import "TaskDetailController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TaskTableHelper : NSObject <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) UINavigationController* navController;
@property (nonatomic, strong) UIViewController* viewController;

- (TaskTableHelper*)initWithTableViewAndController:(UITableView*)tableView viewController:(UIViewController*)viewController navController:(UINavigationController*)navController;

- (void)setNewTaskItems:(NSArray*)items;
- (NSArray*)getItems;

- (void)reloadTableView;

+(TaskTableHelper *)getInstance;
-(void)openTaskDetailController:(Task*)task;

@end

NS_ASSUME_NONNULL_END
