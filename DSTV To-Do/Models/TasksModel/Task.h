//
//  Task.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Task : NSObject

@property (readwrite) NSNumber* id;
@property (readwrite) int userID;
@property (nonatomic, strong) NSString* description;
@property (assign) BOOL isComplete;
@property (assign) BOOL isImportant;
@property (assign) NSString* createdDate;
@property (assign) NSString* reminderDate;



@end

NS_ASSUME_NONNULL_END
