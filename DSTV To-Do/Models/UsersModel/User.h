//
//  User.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface User : NSObject

@property (readonly) NSNumber* id;
@property (nonatomic, strong) NSString* userEmailOrCell;

@end

NS_ASSUME_NONNULL_END
