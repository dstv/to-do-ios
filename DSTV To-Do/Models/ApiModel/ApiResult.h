//
//  ApiResult.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApiResult : NSObject

@property (readonly) int status;
@property (readonly) NSString* message;
@property (nonatomic, strong) NSDictionary* data;


@end

NS_ASSUME_NONNULL_END
