//
//  ApiResponse.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiResult.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApiResponse : NSObject

@property (nonatomic, strong, nullable) NSString* error;
@property (nonatomic, strong, nullable) ApiResult* result;


@end

NS_ASSUME_NONNULL_END
