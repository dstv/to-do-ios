//
//  ConnectionHelper.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking/AFNetworking.h"


NS_ASSUME_NONNULL_BEGIN

@interface ConnectionHelper : NSObject

+(void)GET:(NSString*) url completionBlock:(void (^)(id responseObject, NSError * error))handler;
+(void)POST:(NSString*)url postObject:(NSDictionary*)postObject completionBlock:(void (^)(id responseObject, NSError * error))handler;
+(NSDictionary*)getObjectFromNSError:(NSError*)error;
+(NSString*)getObjectMessageFromNSError:(NSError*)error;

@end

NS_ASSUME_NONNULL_END
