//
//  ConnectionHelper.m
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/23.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import "ConnectionHelper.h"

@implementation ConnectionHelper


+(void)GET:(NSString*)url completionBlock:(void (^)(id responseObject, NSError * error))handler {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:url
      parameters:nil
        progress:nil
         success:^(NSURLSessionDataTask*  task, id responseObject) {
        handler(responseObject, nil);
    }
         failure:^(NSURLSessionDataTask* task, NSError* error) {
        handler(nil, error);
    }
     ];
}

+(void)POST:(NSString*)url postObject:(NSDictionary*)postObject completionBlock:(void (^)(id responseObject, NSError * error))handler {
    NSURL *URL = [NSURL URLWithString:url];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:URL.absoluteString
       parameters:postObject
         progress:nil
          success:^(NSURLSessionTask *task, id responseObject) {
        handler(responseObject, nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        handler(nil, error);
    }
     ];
}

+(NSDictionary*)getObjectFromNSError:(NSError*)error {
    NSError *jsonError;
    NSData* errorData = [error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"];
    NSDictionary* jsonDictionary = [NSJSONSerialization JSONObjectWithData:errorData options:kNilOptions error:&jsonError];
    
    return jsonDictionary;
}

+(NSString*)getObjectMessageFromNSError:(NSError*)error {
    
    NSError *jsonError;
    NSData* errorData = [error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"];
    if (error == nil || errorData == nil) {
        return @"Something went wrong, please try again later";
    }
    
    NSDictionary* jsonDictionary = [NSJSONSerialization JSONObjectWithData:errorData options:kNilOptions error:&jsonError];
    
    if (jsonDictionary != nil) {
        return [jsonDictionary objectForKey:@"resultMessage"];
    }
    return @"Something went wrong, please try again later";
    
}



@end
