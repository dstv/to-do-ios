//
//  AppDelegate.h
//  DSTV To-Do
//
//  Created by Jzel Theophiluis on 2020/05/20.
//  Copyright © 2020 DSTV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

